package com.example.fullsearchwiki.repo;

import com.example.fullsearchwiki.dto.AbstractDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface DocumentsRepo extends ElasticsearchRepository<AbstractDocument, Integer> {
}
