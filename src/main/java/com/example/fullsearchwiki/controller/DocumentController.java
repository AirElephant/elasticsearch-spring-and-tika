package com.example.fullsearchwiki.controller;

import com.example.fullsearchwiki.dto.AbstractDocument;
import com.example.fullsearchwiki.service.DocumentService;
import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class DocumentController {
    @Autowired
    private DocumentService documentService;

    @GetMapping("/findAll")
    Iterable<AbstractDocument> findAll() {
        return documentService.getDocuments();
    }

    @PostMapping("/insert")
    public AbstractDocument insertDocument(@RequestBody AbstractDocument document) {
        return documentService.insertDocument(document);
    }

    @PostMapping("/parse")
    public ResponseEntity<String> parseDocument(@RequestParam("file") MultipartFile file) {
        try {
            String parsedText = documentService.parseDocument(file);
            return ResponseEntity.ok(parsedText);
        } catch (IOException | TikaException e) {
            return ResponseEntity.status(500).body("Ошибка при парсинге документа: " + e.getMessage());
        }
    }

    @PostMapping("/search")
    public ResponseEntity<String> searchInDocument(
            @RequestParam("file") MultipartFile file,
            @RequestParam("textToSearch") String textToSearch) {
        try {
            boolean found = documentService.searchInDocument(textToSearch, file);
            if (found) {
                return ResponseEntity.ok("Текст " + "`" + textToSearch + "`" + " найден.");
            } else {
                return ResponseEntity.ok("Текст не найден в документе.");
            }
        } catch (IOException e) {
            return ResponseEntity.status(500).body("Ошибка при поиске в документе: " + e.getMessage());
        } catch (TikaException e) {
            throw new RuntimeException(e);
        }
    }
}
