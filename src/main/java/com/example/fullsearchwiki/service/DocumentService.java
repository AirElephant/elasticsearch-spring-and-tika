package com.example.fullsearchwiki.service;

import com.example.fullsearchwiki.dto.AbstractDocument;
import com.example.fullsearchwiki.repo.DocumentsRepo;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class DocumentService {
    @Autowired
    private DocumentsRepo documentsRepo;

    public Iterable<AbstractDocument> getDocuments() {
        return documentsRepo.findAll();
    }

    public AbstractDocument insertDocument(AbstractDocument document) {
        return documentsRepo.save(document);
    }

    public AbstractDocument updateDocument(AbstractDocument document, int id) {
        AbstractDocument document1 = documentsRepo.findById(id).get();
        document1.setBody(document.getBody());
        return document1;
    }
    public String parseDocument(MultipartFile file) throws IOException, TikaException {
        Tika tika = new Tika();
        return tika.parseToString(file.getInputStream());
    }

    public boolean searchInDocument(String textToSearch, MultipartFile file) throws IOException, TikaException {
        String parsedText = parseDocument(file);
        return parsedText.contains(textToSearch);
    }
}
