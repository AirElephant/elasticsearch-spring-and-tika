package com.example.fullsearchwiki.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(indexName = "wiki_item")
public class AbstractDocument {
    private int id;
    private String body;
    private String dataType;
}
