package com.example.fullsearchwiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullsearchWikiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FullsearchWikiApplication.class, args);
    }

}
